﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace blackJack
{
    class Game
    {
        private Deck gameDeck;
        private Card[] playerCards;
        private Card[] compCards;

        private void setCards()
        {
            for (int i = 0; i < 20; i++)
            {
                playerCards[i] = gameDeck.deck[i];
            }

            for (int i = 20; i < 40; i++)
            {
                compCards[i - 20] = gameDeck.deck[i];
            }
        }

        public Game()
        {
            gameDeck = new Deck();
            playerCards = new Card[20];
            compCards = new Card[20];

            setCards();
        }

        public int[] returnPlayerCardValues()
        {
            int[] playerCardValues = new int[20];
            for (int i = 0; i < 20; i++)
            {
                playerCardValues[i] = playerCards[i].returnValue();
            }
            return playerCardValues;
        }

        public int[] returnCompCardValues()
        {
            int[] compCardValues = new int[20];
            for (int i = 0; i < 20; i++)
            {
                compCardValues[i] = compCards[i].returnValue();
            }
            return compCardValues;
        }

        //Player Card Picture

        public string[] returnPlayerCardPic()
        {
            string[] playerCardLoc = new string[20];
            for (int i = 0; i < 20; i++)
            {
                playerCardLoc[i] = playerCards[i].returnPicture();
            }
            return playerCardLoc;
        }

        //Computer Card Picture

        public string[] returnCompCardPic()
        {
            string[] compCardLoc = new string[20];
            for (int i = 0; i < 20; i++)
            {
                compCardLoc[i] = compCards[i].returnPicture();
            }
            return compCardLoc;
        }

        public void newHand()
        {
            gameDeck.shuffle();
            setCards();
        }
    }
}
