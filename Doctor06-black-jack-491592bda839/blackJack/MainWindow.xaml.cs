﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;

namespace blackJack
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private delegate void DisplayMessageDelegate();
        static Game game = new Game();
        static Chips chips = new Chips();

        int[] playerCardValues = game.returnPlayerCardValues();
        int[] compCardValues = game.returnCompCardValues();
        String[] playerCardPic = game.returnPlayerCardPic();
        String[] compCardPic = game.returnCompCardPic();
        BitmapImage[] playerCard = new BitmapImage[10];
        BitmapImage[] compCard = new BitmapImage[10];
        

        int numplayerCards;
        int numCompCards;
        int playerTotal;
        int compTotal;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void hitBtn_Click(object sender, RoutedEventArgs e)
        {
            //if there are only two cards, give the player a 3rd in slot #6
            if (numplayerCards == 2)
            {
                playerCardPos6.Source = playerCard[2];

            }
            else if (numplayerCards == 3)
            {
                playerCardPos3.Source = playerCardPos4.Source;
                playerCardPos4.Source = playerCardPos5.Source;
                playerCardPos5.Source = playerCardPos6.Source;
                playerCardPos6.Source = playerCard[3];
            }
            else if (numplayerCards == 4)
            {
                playerCardPos7.Source = playerCard[4];
            }
            else if (numplayerCards == 5)
            {
                playerCardPos2.Source = playerCardPos3.Source;
                playerCardPos3.Source = playerCardPos4.Source;
                playerCardPos4.Source = playerCardPos5.Source;
                playerCardPos5.Source = playerCardPos6.Source;
                playerCardPos6.Source = playerCardPos7.Source;
                playerCardPos7.Source = playerCard[5];
            }
            else if (numplayerCards == 6)
            {
                playerCardPos8.Source = playerCard[6];
            }
            else if (numplayerCards == 7)
            {
                playerCardPos1.Source = playerCardPos2.Source;
                playerCardPos2.Source = playerCardPos3.Source;
                playerCardPos3.Source = playerCardPos4.Source;
                playerCardPos4.Source = playerCardPos5.Source;
                playerCardPos5.Source = playerCardPos6.Source;
                playerCardPos6.Source = playerCardPos7.Source;
                playerCardPos7.Source = playerCardPos8.Source;
                playerCardPos8.Source = playerCard[7];
            }
            else if (numplayerCards == 8)
            {
                playerCardPos9.Source = playerCard[8];
            }
            else if (numplayerCards == 9)
            {
                playerCardPos0.Source = playerCardPos1.Source;
                playerCardPos1.Source = playerCardPos2.Source;
                playerCardPos2.Source = playerCardPos3.Source;
                playerCardPos3.Source = playerCardPos4.Source;
                playerCardPos4.Source = playerCardPos5.Source;
                playerCardPos5.Source = playerCardPos6.Source;
                playerCardPos6.Source = playerCardPos7.Source;
                playerCardPos7.Source = playerCardPos8.Source;
                playerCardPos8.Source = playerCardPos9.Source;
                playerCardPos9.Source = playerCard[9];
            }
            numplayerCards++;
            playerCardTotalValueLbl.Content = getPlayerTotal(numplayerCards);
            if (is_bust(getPlayerTotal(numplayerCards)))
            {
                hitBtn.Visibility = Visibility.Collapsed;
                dealerWonLbl.Visibility = Visibility.Visible;
                stayBtn.Visibility = Visibility.Collapsed;
                bustLbl.Margin = new Thickness(0, 0, 0, 0);
                bustLbl.Visibility = Visibility.Visible;
                //restart game
                Thread restartGame = new Thread(new ThreadStart(threadRestartGame));
                restartGame.Start();
            }
        }

        private void placeBetBtn_Click(object sender, RoutedEventArgs e)
        {
            if (chips.returnPlayerWallet() == 0)
            {
                dealerWonLbl.Visibility = Visibility.Visible;
                stayBtn.Visibility = Visibility.Collapsed;
                gameOverLbl.Margin = new Thickness(0, 0, 0, 0);
                gameOverLbl.Visibility = Visibility.Visible;
            }
            else
            {
                //remove bet from wallet
                chips.placeBet();
                updateWallet();
                updateWaletLbl();

                //Clear all card images

                clearTable();
                initCardPics();
                //Hide and show various labels and buttons
                //Bet is locked in and hit and stay buttons become visible

                placeBetBtn.Visibility = Visibility.Collapsed;
                placeBetToBeginLbl.Visibility = Visibility.Collapsed;
                hitBtn.Visibility = Visibility.Visible;
                stayBtn.Visibility = Visibility.Visible;
                btnAddBet.Opacity = .5;
                btnMinusBet.Opacity = .5;
                btnAddBet.IsEnabled = false;
                btnMinusBet.IsEnabled = false;

                //Create blank card to generate back of card image

                Card back = new Card("0");

                //Card image slots are filled middle first and will shift based on number of cards dealt

                playerCardPos4.Source = playerCard[0];
                playerCardPos5.Source = playerCard[1];

                //Number of player cards is 2 -- we will need this count variable later
                numplayerCards = 2;
                if (getPlayerTotal(numplayerCards) == 21)
                {
                    hitBtn.IsEnabled = false;
                    stayBtn.IsEnabled = false;
                    blackJackLbl.Visibility = Visibility.Visible;

                    chips.playerWon();
                    updateWallet();
                    updateWaletLbl();

                    //restart game
                    Thread restartGame = new Thread(new ThreadStart(threadRestartGame));
                    restartGame.Start();
                }

                compCardPos4.Source = new BitmapImage(new Uri(back.returnPicture(), UriKind.RelativeOrAbsolute)); //This spot's value will be compCardPic[0]
                compCardPos5.Source = compCard[1];

                //Number of computer cards is 2 -- we will need this count variable later
                numCompCards = 2;
                playerCardTotalValueLbl.Content = getPlayerTotal(numplayerCards);
            }
        }

        private void stayBtn_Click(object sender, RoutedEventArgs e)
        {
            hitBtn.Visibility = Visibility.Collapsed;
            stayBtn.Visibility = Visibility.Collapsed;
            //Thread MyThread = new Thread(new ThreadStart(threadMethod));
            //MyThread.Start();
            compCardPos4.Source = compCard[0];
            dealerCardTotalValueLbl.Content = getCompTotal(numCompCards);

            if (is_bust(getCompTotal(numCompCards)))
            {
                hitBtn.IsEnabled = false;
                youWonLbl.Visibility = Visibility.Visible;
                bustLbl.Margin = new Thickness(0, -300, 0, 0);
                bustLbl.Visibility = Visibility.Visible;
                stayBtn.IsEnabled = false;
                //restart game
                Thread restartGame = new Thread(new ThreadStart(threadRestartGame));
                restartGame.Start();
            }
            else
            {
                if (!is_dealer_limit(getCompTotal(numCompCards)))
                {
                    Thread MyThread = new Thread(new ThreadStart(threadMethod));
                    MyThread.Start();
                }
                else
                {
                    whoWon(getCompTotal(numCompCards), getPlayerTotal(numplayerCards));
                    Thread restartGame = new Thread(new ThreadStart(threadRestartGame));
                    restartGame.Start();
                }
            }
        }
        public void doWork()
        {
            if (numCompCards == 2)
            {
                compCardPos6.Source = compCard[2];

            }
            else if (numCompCards == 3)
            {
                compCardPos3.Source = compCardPos4.Source;
                compCardPos4.Source = compCardPos5.Source;
                compCardPos5.Source = compCardPos6.Source;
                compCardPos6.Source = compCard[3];
            }
            else if (numCompCards == 4)
            {
                compCardPos7.Source = compCard[4];
            }
            else if (numCompCards == 5)
            {
                compCardPos2.Source = compCardPos3.Source;
                compCardPos3.Source = compCardPos4.Source;
                compCardPos4.Source = compCardPos5.Source;
                compCardPos5.Source = compCardPos6.Source;
                compCardPos6.Source = compCardPos7.Source;
                compCardPos7.Source = compCard[5];
            }
            else if (numCompCards == 6)
            {
                compCardPos8.Source = compCard[6];
            }
            else if (numCompCards == 7)
            {
                compCardPos1.Source = compCardPos2.Source;
                compCardPos2.Source = compCardPos3.Source;
                compCardPos3.Source = compCardPos4.Source;
                compCardPos4.Source = compCardPos5.Source;
                compCardPos5.Source = compCardPos6.Source;
                compCardPos6.Source = compCardPos7.Source;
                compCardPos7.Source = compCardPos8.Source;
                compCardPos8.Source = compCard[7];
            }
            else if (numCompCards == 8)
            {
                compCardPos9.Source = playerCard[8];
            }
            else if (numCompCards == 9)
            {
                compCardPos0.Source = compCardPos1.Source;
                compCardPos1.Source = compCardPos2.Source;
                compCardPos2.Source = compCardPos3.Source;
                compCardPos3.Source = compCardPos4.Source;
                compCardPos4.Source = compCardPos5.Source;
                compCardPos5.Source = compCardPos6.Source;
                compCardPos6.Source = compCardPos7.Source;
                compCardPos7.Source = compCardPos8.Source;
                compCardPos8.Source = compCardPos9.Source;
                compCardPos9.Source = compCard[9];
            }
            numCompCards++;
            dealerCardTotalValueLbl.Content = getCompTotal(numCompCards);
            if (is_bust(getCompTotal(numCompCards)))
            {
                chips.playerWon();
                updateWallet();
                updateWaletLbl();

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                hitBtn.IsEnabled = false;
                youWonLbl.Visibility = Visibility.Visible;
                bustLbl.Margin = new Thickness(0, -300, 0, 0);
                bustLbl.Visibility = Visibility.Visible;
                stayBtn.IsEnabled = false;
                //restart game
                Thread restartGame = new Thread(new ThreadStart(threadRestartGame));
                restartGame.Start();
            }
            else
            {
                if (!is_dealer_limit(getCompTotal(numCompCards)))
                {
                    Thread MyThread = new Thread(new ThreadStart(threadMethod));
                    MyThread.Start();
                }
                else
                {
                    whoWon(getCompTotal(numCompCards), getPlayerTotal(numplayerCards));
                    Thread restartGame = new Thread(new ThreadStart(threadRestartGame));
                    restartGame.Start();
                }
            }
        }

        //Helper methods to keep above event code cleaner

        private void clearTable()
        {
            playerCardPos0.Source = null;
            playerCardPos1.Source = null;
            playerCardPos2.Source = null;
            playerCardPos3.Source = null;
            playerCardPos4.Source = null;
            playerCardPos5.Source = null;
            playerCardPos6.Source = null;
            playerCardPos7.Source = null;
            playerCardPos8.Source = null;
            playerCardPos9.Source = null;

            compCardPos0.Source = null;
            compCardPos1.Source = null;
            compCardPos2.Source = null;
            compCardPos3.Source = null;
            compCardPos4.Source = null;
            compCardPos5.Source = null;
            compCardPos6.Source = null;
            compCardPos7.Source = null;
            compCardPos8.Source = null;
            compCardPos9.Source = null;

            for (int i = 0; i < 10; i++)
            {
                playerCard[i] = null;
                compCard[i] = null;

            }
        }

        private void restartGame()
        {
            youWonLbl.Visibility = Visibility.Hidden;
            dealerWonLbl.Visibility = Visibility.Hidden;
            bustLbl.Visibility = Visibility.Hidden;
            drawLbl.Visibility = Visibility.Hidden;
            blackJackLbl.Visibility = Visibility.Hidden;
            dealerCardTotalValueLbl.Content = '?';
            playerCardTotalValueLbl.Content = '0';
            placeBetBtn.Visibility = Visibility.Visible;
            btnMinusBet.Opacity = 100;
            btnAddBet.Opacity = 100;
            btnAddBet.IsEnabled = true;
            btnMinusBet.IsEnabled = true;
            hitBtn.IsEnabled = true;
            stayBtn.IsEnabled = true;

            chips.newHand();
            game.newHand();

            updateWallet();

            playerCardValues = game.returnPlayerCardValues();
            compCardValues = game.returnCompCardValues();
            playerCardPic = game.returnPlayerCardPic();
            compCardPic = game.returnCompCardPic();
        }

        public void initCardPics()
        {
            for (int i = 0; i < 10; i++)
            {
                playerCard[i] = new BitmapImage(new Uri(playerCardPic[i], UriKind.RelativeOrAbsolute));
                compCard[i] = new BitmapImage(new Uri(compCardPic[i], UriKind.RelativeOrAbsolute));
                
            }
        }

        public int getCompTotal(int compCount)
        {
            compTotal = 0;
            for (int i = 0; i < compCount; i++)
            {
                compTotal += compCardValues[i];
            }

            for (int i = 0; i < compCount; i++)
            {
                if (compCardValues[i] == 1)
                {
                    if ((compTotal + 10) <= 21)
                    {
                        compTotal += 10;
                    }
                }
            }

            return compTotal;
        }

        public int getPlayerTotal(int playerCount)
        {
            playerTotal = 0;
            for(int i = 0; i < playerCount; i++){
                playerTotal += playerCardValues[i];
            }

            for (int i = 0; i < playerCount; i++)
            {
                if (playerCardValues[i] == 1)
                {
                    if ((playerTotal + 10) <= 21)
                    {
                        playerTotal += 10;
                    }
                }
            }

            return playerTotal;
        }

        public bool is_bust(int total)
        {
            if (total > 21)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool is_dealer_limit(int limit)
        {
            if (limit >= 17)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private void whoWon(int dealer, int player)
        {
            if (dealer > player)
            {
                dealerWonLbl.Visibility = Visibility.Visible;
            }
            else if (dealer == player)
            {
                drawLbl.Visibility = Visibility.Visible;
                chips.draw();
                
                //update walletLbl
                updateWaletLbl();
            }
            else
            {
                youWonLbl.Visibility = Visibility.Visible;
                chips.playerWon();
                
                //update wallet
                int wallet = chips.returnPlayerWallet();
                walletAmountLbl.Content = wallet.ToString();
            }
        }

        //disaster of methods... dont judge
        private void threadMethod()
        {
            Thread.Sleep(2000);
            this.Dispatcher.BeginInvoke(new DisplayMessageDelegate(doWork));
        }
        private void threadRestartGame()
        {
            Thread.Sleep(2000);
            this.Dispatcher.BeginInvoke(new DisplayMessageDelegate(clearTable));
            this.Dispatcher.BeginInvoke(new DisplayMessageDelegate(restartGame));
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            //decrement bet
            chips.decreaseBet();
            updateWallet();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            //increment bet
            chips.increaseBet();
            updateWallet();
        }

        public void updateWallet()
        {
            int betAmount = chips.returnCurrentBet();

            betAmountLbl.Content = betAmount.ToString();
        }

        public void updateWaletLbl()
        {
            int wallet = chips.returnPlayerWallet();
            walletAmountLbl.Content = wallet.ToString();
        }
    }
}
