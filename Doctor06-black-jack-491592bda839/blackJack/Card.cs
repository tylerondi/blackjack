﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace blackJack
{
    class Card
    {
        private int value;
        private string picture;

        public Card(String display)
        {
            if (display.Equals("AH"))
            {
                value = 1;
                picture = "\\Assets\\h1.png";
            }
            else if (display.Equals("AS"))
            {
                value = 1;
                picture = "\\Assets\\s1.png";
            }
            else if (display.Equals("AC"))
            {
                value = 1;
                picture = "\\Assets\\c1.png";
            }
            else if (display.Equals("AD"))
            {
                value = 1;
                picture = "\\Assets\\d1.png";
            }
            else if (display.Equals("KH"))
            {
                value = 10;
                picture = "\\Assets\\hk.png";
            }
            else if (display.Equals("KS"))
            {
                value = 10;
                picture = "\\Assets\\sk.png";
            }
            else if (display.Equals("KC"))
            {
                value = 10;
                picture = "\\Assets\\ck.png";
            }
            else if (display.Equals("KD"))
            {
                value = 10;
                picture = "\\Assets\\dk.png";
            }
            else if (display.Equals("QH"))
            {
                value = 10;
                picture = "\\Assets\\hq.png";
            }
            else if (display.Equals("QS"))
            {
                value = 10;
                picture = "\\Assets\\sq.png";
            }
            else if (display.Equals("QC"))
            {
                value = 10;
                picture = "\\Assets\\cq.png";
            }
            else if (display.Equals("QD"))
            {
                value = 10;
                picture = "\\Assets\\dq.png";
            }
            else if (display.Equals("JH"))
            {
                value = 10;
                picture = "\\Assets\\hj.png";
            }
            else if (display.Equals("JS"))
            {
                value = 10;
                picture = "\\Assets\\sj.png";
            }
            else if (display.Equals("JC"))
            {
                value = 10;
                picture = "\\Assets\\cj.png";
            }
            else if (display.Equals("JD"))
            {
                value = 10;
                picture = "\\Assets\\dj.png";
            }
            else if (display.Equals("10H"))
            {
                value = 10;
                picture = "\\Assets\\h10.png";
            }
            else if (display.Equals("10S"))
            {
                value = 10;
                picture = "\\Assets\\s10.png";
            }
            else if (display.Equals("10C"))
            {
                value = 10;
                picture = "\\Assets\\c10.png";
            }
            else if (display.Equals("10D"))
            {
                value = 10;
                picture = "\\Assets\\d10.png";
            }
            else if (display.Equals("9H"))
            {
                value = 9;
                picture = "\\Assets\\h9.png";
            }
            else if (display.Equals("9S"))
            {
                value = 9;
                picture = "\\Assets\\s9.png";
            }
            else if (display.Equals("9C"))
            {
                value = 9;
                picture = "\\Assets\\c9.png";
            }
            else if (display.Equals("9D"))
            {
                value = 9;
                picture = "\\Assets\\d9.png";
            }
            else if (display.Equals("8H"))
            {
                value = 8;
                picture = "\\Assets\\h8.png";
            }
            else if (display.Equals("8S"))
            {
                value = 8;
                picture = "\\Assets\\s8.png";
            }
            else if (display.Equals("8C"))
            {
                value = 8;
                picture = "\\Assets\\c8.png";

            }
            else if (display.Equals("8D"))
            {
                value = 8;
                picture = "\\Assets\\d8.png";
            }
            else if (display.Equals("7H"))
            {
                value = 7;
                picture = "\\Assets\\h7.png";
            }
            else if (display.Equals("7S"))
            {
                value = 7;
                picture = "\\Assets\\s7.png";
            }
            else if (display.Equals("7C"))
            {
                value = 7;
                picture = "\\Assets\\c7.png";
            }
            else if (display.Equals("7D"))
            {
                value = 7;
                picture = "\\Assets\\d7.png";
            }
            else if (display.Equals("6H"))
            {
                value = 6;
                picture = "\\Assets\\h6.png";
            }
            else if (display.Equals("6S"))
            {
                value = 6;
                picture = "\\Assets\\s6.png";
            }
            else if (display.Equals("6C"))
            {
                value = 6;
                picture = "\\Assets\\c6.png";
            }
            else if (display.Equals("6D"))
            {
                value = 6;
                picture = "\\Assets\\d6.png";
            }
            else if (display.Equals("5H"))
            {
                value = 5;
                picture = "\\Assets\\h5.png";
            }
            else if (display.Equals("5S"))
            {
                value = 5;
                picture = "\\Assets\\s5.png";
            }
            else if (display.Equals("5C"))
            {
                value = 5;
                picture = "\\Assets\\c5.png";
            }
            else if (display.Equals("5D"))
            {
                value = 5;
                picture = "\\Assets\\d5.png";
            }
            else if (display.Equals("4H"))
            {
                value = 4;
                picture = "\\Assets\\h4.png";
            }
            else if (display.Equals("4S"))
            {
                value = 4;
                picture = "\\Assets\\s4.png";
            }
            else if (display.Equals("4C"))
            {
                value = 4;
                picture = "\\Assets\\c4.png";
            }
            else if (display.Equals("4D"))
            {
                value = 4;
                picture = "\\Assets\\d4.png";
            }
            else if (display.Equals("3H"))
            {
                value = 3;
                picture = "\\Assets\\h3.png";
            }
            else if (display.Equals("3S"))
            {
                value = 3;
                picture = "\\Assets\\s3.png";
            }
            else if (display.Equals("3C"))
            {
                value = 3;
                picture = "\\Assets\\c3.png";
            }
            else if (display.Equals("3D"))
            {
                value = 3;
                picture = "\\Assets\\d3.png";
            }
            else if (display.Equals("2H"))
            {
                value = 2;
                picture = "\\Assets\\h2.png";
            }
            else if (display.Equals("2S"))
            {
                value = 2;
                picture = "\\Assets\\s2.png";
            }
            else if (display.Equals("2C"))
            {
                value = 2;
                picture = "\\Assets\\c2.png";
            }
            else if (display.Equals("2D"))
            {
                value = 2;
                picture = "\\Assets\\d2.png";
            }
            else
            {
                value = 0;
                picture = "\\Assets\\b1fv.png";
            }
        }

        public int returnValue()
        {
            return value;
        }

        public string returnPicture()
        {
            return picture;
        }
    }
}
