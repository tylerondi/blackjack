﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace blackJack
{
    class Deck
    {
        public Card[] deck;

        public void shuffle()
        {
            Random random = new Random();
            int randomNumber = 0;

            Card temp = new Card("temp");

            //shuffle every card in array
            for (int i = 0; i < 52; i++)
            {
                randomNumber = random.Next(0, 52);
                temp = deck[randomNumber];
                deck[randomNumber] = deck[i];
                deck[i] = temp;
            }
        }

        public Deck()
        {
            deck = new Card[52];
            //setup the deck
            deck[0] = new Card("AS");
            deck[1] = new Card("AD");
            deck[2] = new Card("AC");
            deck[3] = new Card("AH");
            deck[4] = new Card("KS");
            deck[5] = new Card("KD");
            deck[6] = new Card("KC");
            deck[7] = new Card("KH");
            deck[8] = new Card("QS");
            deck[9] = new Card("QD");
            deck[10] = new Card("QC");
            deck[11] = new Card("QH");
            deck[12] = new Card("JS");
            deck[13] = new Card("JD");
            deck[14] = new Card("JC");
            deck[15] = new Card("JH");
            deck[16] = new Card("10S");
            deck[17] = new Card("10D");
            deck[18] = new Card("10C");
            deck[19] = new Card("10H");
            deck[20] = new Card("9S");
            deck[21] = new Card("9D");
            deck[22] = new Card("9C");
            deck[23] = new Card("9H");
            deck[24] = new Card("8S");
            deck[25] = new Card("8C");
            deck[26] = new Card("8D");
            deck[27] = new Card("8H");
            deck[28] = new Card("7S");
            deck[29] = new Card("7C");
            deck[30] = new Card("7D");
            deck[31] = new Card("7H");
            deck[32] = new Card("6S");
            deck[33] = new Card("6C");
            deck[34] = new Card("6D");
            deck[35] = new Card("6H");
            deck[36] = new Card("5S");
            deck[37] = new Card("5C");
            deck[38] = new Card("5D");
            deck[39] = new Card("5H");
            deck[40] = new Card("4S");
            deck[41] = new Card("4D");
            deck[42] = new Card("4C");
            deck[43] = new Card("4H");
            deck[44] = new Card("3S");
            deck[45] = new Card("3C");
            deck[46] = new Card("3D");
            deck[47] = new Card("3H");
            deck[48] = new Card("2S");
            deck[49] = new Card("2C");
            deck[50] = new Card("2D");
            deck[51] = new Card("2H");

            shuffle();
        }
    }
}
