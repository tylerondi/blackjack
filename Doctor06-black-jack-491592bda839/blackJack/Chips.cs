﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace blackJack
{
    class Chips
    {
        private int playerWallet;
        private int currentBet;

        public void reset()
        {
            playerWallet = 100;
            currentBet = 10;
        }

        public void newHand()
        {
            if (currentBet > playerWallet)
            {
                currentBet = playerWallet;
            }
        }

        public Chips()
        {
            reset();
        }

        public void placeBet()
        {
            playerWallet -= currentBet;
        }

        public void increaseBet()
        {
            //do you have enough money to bet that?
            if ((playerWallet - currentBet - 10) < 0)
            {
                //do nothing
            }
            else
            {
                currentBet += 10;
            }
        }

        public void decreaseBet()
        {
            if(currentBet - 10 <= 0)
            {
                //do nothing
            }
            else
            {
                currentBet -= 10;
            }
        }
        
        public void playerWon()
        {
            playerWallet += (currentBet * 2);
        }
        
        public void draw()
        {
            playerWallet += currentBet;
        }

        public int returnPlayerWallet()
        {
            return playerWallet;
        }

        public int returnCurrentBet()
        {
            return currentBet;
        }
    }
}
