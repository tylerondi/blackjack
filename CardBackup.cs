﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlackJackWindows
{
    class Card
    {
        private int value;
        private string picture;

        public Card(String display)
        {
            if (display.Equals("AH"))
            {
                value = -1;
                picture = "cards\\3.png";
            }
            else if (display.Equals("AS"))
            {
                value = -1;
                picture = "cards\\2.png";
            }
            else if (display.Equals("AC"))
            {
                value = -1;
                picture = "cards\\1.png";
            }
            else if (display.Equals("AD"))
            {
                value = -1;
                picture = "cards\\4.png";
            }
            else if (display.Equals("KH"))
            {
                value = 10;
                picture = "cards\\7.png";
            }
            else if (display.Equals("KS"))
            {
                value = 10;
                picture = "cards\\6.png";
            }
            else if (display.Equals("KC"))
            {
                value = 10;
                picture = "cards\\5.png";
            }
            else if (display.Equals("KD"))
            {
                value = 10;
                picture = "cards\\8.png";
            }
            else if (display.Equals("QH"))
            {
                value = 10;
                picture = "cards\\11.png";
            }
            else if (display.Equals("QS"))
            {
                value = 10;
                picture = "cards\\10.png";
            }
            else if (display.Equals("QC"))
            {
                value = 10;
                picture = "cards\\9.png";
            }
            else if (display.Equals("QD"))
            {
                value = 10;
                picture = "cards\\12.png";
            }
            else if (display.Equals("JH"))
            {
                value = 10;
                picture = "cards\\15.png";
            }
            else if (display.Equals("JS"))
            {
                value = 10;
                picture = "cards\\14.png";
            }
            else if (display.Equals("JC"))
            {
                value = 10;
                picture = "cards\\13.png";
            }
            else if (display.Equals("JD"))
            {
                value = 10;
                picture = "cards\\16.png";
            }
            else if (display.Equals("10H"))
            {
                value = 10;
                picture = "cards\\19.png";
            }
            else if (display.Equals("10S"))
            {
                value = 10;
                picture = "cards\\18.png";
            }
            else if (display.Equals("10C"))
            {
                value = 10;
                picture = "cards\\17.png";
            }
            else if (display.Equals("10D"))
            {
                value = 10;
                picture = "cards\\20.png";
            }
            else if (display.Equals("9H"))
            {
                value = 9;
                picture = "cards\\23.png";
            }
            else if (display.Equals("9S"))
            {
                value = 9;
                picture = "cards\\22.png";
            }
            else if (display.Equals("9C"))
            {
                value = 9;
                picture = "cards\\21.png";
            }
            else if (display.Equals("9D"))
            {
                value = 9;
                picture = "cards\\24.png";
            }
            else if (display.Equals("8H"))
            {
                value = 8;
                picture = "cards\\27.png";
            }
            else if (display.Equals("8S"))
            {
                value = 8;
                picture = "cards\\26.png";
            }
            else if (display.Equals("8C"))
            {
                value = 8;
                picture = "cards\\25.png";

            }
            else if (display.Equals("8D"))
            {
                value = 8;
                picture = "cards\\28.png";
            }
            else if (display.Equals("7H"))
            {
                value = 7;
                picture = "cards\\31.png";
            }
            else if (display.Equals("7S"))
            {
                value = 7;
                picture = "cards\\30.png";
            }
            else if (display.Equals("7C"))
            {
                value = 7;
                picture = "cards\\29.png";
            }
            else if (display.Equals("7D"))
            {
                value = 7;
                picture = "cards\\32.png";
            }
            else if (display.Equals("6H"))
            {
                value = 6;
                picture = "cards\\35.png";
            }
            else if (display.Equals("6S"))
            {
                value = 6;
                picture = "cards\\34.png";
            }
            else if (display.Equals("6C"))
            {
                value = 6;
                picture = "cards\\33.png";
            }
            else if (display.Equals("6D"))
            {
                value = 6;
                picture = "cards\\36.png";
            }
            else if (display.Equals("5H"))
            {
                value = 5;
                picture = "cards\\39.png";
            }
            else if (display.Equals("5S"))
            {
                value = 5;
                picture = "cards\\38.png";
            }
            else if (display.Equals("5C"))
            {
                value = 5;
                picture = "cards\\37.png";
            }
            else if (display.Equals("5D"))
            {
                value = 5;
                picture = "cards\\40.png";
            }
            else if (display.Equals("4H"))
            {
                value = 4;
                picture = "cards\\43.png";
            }
            else if (display.Equals("4S"))
            {
                value = 4;
                picture = "cards\\42.png";
            }
            else if (display.Equals("4C"))
            {
                value = 4;
                picture = "cards\\41.png";
            }
            else if (display.Equals("4D"))
            {
                value = 4;
                picture = "cards\\44.png";
            }
            else if (display.Equals("3H"))
            {
                value = 3;
                picture = "cards\\47.png";
            }
            else if (display.Equals("3S"))
            {
                value = 3;
                picture = "cards\\46.png";
            }
            else if (display.Equals("3C"))
            {
                value = 3;
                picture = "cards\\45.png";
            }
            else if (display.Equals("3D"))
            {
                value = 3;
                picture = "cards\\48.png";
            }
            else if (display.Equals("2H"))
            {
                value = 2;
                picture = "cards\\51.png";
            }
            else if (display.Equals("2S"))
            {
                value = 2;
                picture = "cards\\50.png";
            }
            else if (display.Equals("2C"))
            {
                value = 2;
                picture = "cards\\49.png";
            }
            else if (display.Equals("2D"))
            {
                value = 2;
                picture = "cards\\52.png";
            }
            else
            {
                value = 0;
                picture = "cards\\b1fv.png";
            }
        }

        public int returnValue()
        {
            return value;
        }

        public string returnPicture()
        {
            return picture;
        }
    }
}
